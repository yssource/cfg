#!/bin/sh

# Reload sxhkd
super + Escape
  pkill -USR1 -x sxhkd

# Exit bspwm
alt + shift + q
  bspc quit

# Alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# Swap the current node and the biggest node
super + g
	bspc node -s biggest.local

# Close/kill a window
super + {_,shift } + q
  bspc node -{c,k}

# Toggle music
alt + b
  mpc toggle

# Scratchpad
super + i
  $HOME/.config/bspwm/scratchpad.sh switch-app

super + shift + i
  $HOME/.config/bspwm/scratchpad.sh toggle-flag

super + Return
  urxvtc || termite
  

alt + Return
  termite 

super + d
  dmenu_run

super + {h,j,k,l}
	bspc node -f {west,south,north,east}

super + w
  $BROWSER

super + alt + {0-9}
	mpc -q seek {0-9}0%

super + {_,alt + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

XF86Audio{RaiseVolume,LowerVolume,Mute}
	amixer {-q set Master 5%+,-q set Master 5%-,set Master toggle}

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# Resize nodes
alt + {h,j,k,l}
  bspwm_resize {west,south,north,east}

# Resize windows
#alt + shift + {h,j,k,l}
#  bspwm_resize {west,south,north,east}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

# Music
XF86LaunchA
  mpc toggle

# MPD next song
alt + n
  mpc next

# MPD previous song
alt + p
  mpc prev

#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspwm_smart_move {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {Tab,semicolon}
	bspc {node,desktop} -f next

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel




############################################################################ 
#                                        ▗▄▖       ▗▖                      # 
#                                        ▝▜▌       ▐▌                      # 
#     ▐▙█▙  ▟█▙  █▟█▌▗▟██▖ ▟█▙ ▐▙██▖ ▟██▖ ▐▌       ▐▌▟▛  ▟█▙ ▝█ █▌▗▟██▖    # 
#     ▐▛ ▜▌▐▙▄▟▌ █▘  ▐▙▄▖▘▐▛ ▜▌▐▛ ▐▌ ▘▄▟▌ ▐▌       ▐▙█  ▐▙▄▟▌ █▖█ ▐▙▄▖▘    # 
#     ▐▌ ▐▌▐▛▀▀▘ █    ▀▀█▖▐▌ ▐▌▐▌ ▐▌▗█▀▜▌ ▐▌       ▐▛█▖ ▐▛▀▀▘ ▐█▛  ▀▀█▖    # 
#     ▐█▄█▘▝█▄▄▌ █   ▐▄▄▟▌▝█▄█▘▐▌ ▐▌▐▙▄█▌ ▐▙▄      ▐▌▝▙ ▝█▄▄▌  █▌ ▐▄▄▟▌    # 
#     ▐▌▀▘  ▝▀▀  ▀    ▀▀▀  ▝▀▘ ▝▘ ▝▘ ▀▀▝▘  ▀▀      ▝▘ ▀▘ ▝▀▀   █   ▀▀▀     # 
#     ▐▌                                                      █▌           # 
############################################################################ 

# Brightness keys
XF86MonBrightnessDown
  xbacklight -5

XF86MonBrightnessUp
  xbacklight +5

# Raise Volume
#XF86AudioRaiseVolume
        amixer set Master 10%+


# Lower Volume
#XF86AudioLowerVolume
        amixer set Master 10%-

# Mute 
#XF86AudioMute
        amixer -D pulse set Master 1+ toggle

# Screenshot
Print
    import -window root /tmp/foo.png; cat /tmp/foo.png | xclip -selection clipboard -t image/png

# File Manager
XF86Explorer
    urxvtc -e ranger

## Mouse buttons
#super + button{1-3}
#   bspc pointer -g {move,resize_side,resize_corner}
#
#super + !button{1-3}
#   bspc pointer -t %i %i
#
#super + @button{1-3}
#   bspc pointer -u
#

# Music little backward/forward
alt + bracket{left,right}
  mpc seek {-10,+10}

# Music backward/forward
alt + shift + bracket{left,right}
  mpc seek {-120,+120}
 
# Lock the screen
super + x
  "$HOME"/.local/bin/lock

# vim:ft=sxhkdrc
